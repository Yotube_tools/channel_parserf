import logging

def logger_initiations():
    '''Initializing the logger.'''
    logger = logging.getLogger(__name__)
    logger.setLevel(logging.DEBUG)
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)

    formatter = logging.Formatter(
        '%(asctime)s [%(name)s] %(levelname)-8s %(message)s')
    ch.setFormatter(formatter)
    logger.addHandler(ch)
    return logger

logger = logger_initiations()