FROM python:3.6-alpine3.7

LABEL maintainer="Ihor Kharkhun"

RUN mkdir /channel_parser

COPY ./requirements.txt /channel_parser/requirements.txt

WORKDIR /channel_parser

RUN apk update \
  && apk add --virtual build-deps gcc python3-dev musl-dev \
  && apk add postgresql-dev \
  && pip install -r requirements.txt --no-cache-dir \
  && apk del build-deps

COPY ./app.py /channel_parser/app.py
COPY ./db.py /channel_parser/db.py
COPY ./channel_parser.py /channel_parser/channel_parser.py
COPY ./logger.py /channel_parser/logger.py

CMD ["python3", "app.py"]