from flask import Flask, request
from time import sleep
from channel_parser import link_generator, youtube_request, json_parser, add_to_db, create_rmq_task

app = Flask(__name__)

@app.route('/', methods=['GET', 'POST'])
def insert_channel_id():
    channel_id = request.form['id']

    next_page_token = None
    while True:
        url = link_generator(channel_id, next_page_token)
        response = youtube_request(url)
        videos, next_page_token = json_parser(response)
        videos_bd_id = add_to_db(videos)
        create_rmq_task(videos_bd_id)
        if next_page_token is None:
            break


    return '+++'

if __name__ == '__main__':
   app.run(host = '0.0.0.0', threaded=True)
