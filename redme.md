EN:
Channel_parser is a microservice that stores information about all videos found on the YouTube channel. Channel_parser accepts the yotube id of the channel, processes the request, writes information about the found videos to the database and sends the download task to the Downloader micro service if it is available. The micro-service architecture allows you to run multiple Channel_parser simultaneously to increase the number of concurrently processed queries.

1. The network should have a database in which records will be made about the videos found. The connection parameters must be written to the environment variable DB_CONNECT_STR which is in the env.list file. The format of the variable is: {DB_name}://{user}:{password}@{IP}:{Port}
	An example of running Postgresql in Docker (it is not recommended to run the database in the docker): run --name postgresql -p 0.0.0.0:5432:5432 -e POSTGRES_PASSWORD=1 -d postgres
2. To transfer the found videos to download on the network should be RabbitMQ. The IP address of RabbitMQ needs to be written to the environment variable PIKA_CONNECT_IP. The name of the queue in the variable PIKA_QUEUE_NAME. The variables are in the env.list file
	Example of running RabbitMQ in Docker: docker run -d --hostname my-rabbit --name rabbit -p 0.0.0.0:15672:15672 -p 0.0.0.0:5672:5672 rabbitmq:3-management
3. In the env.list file in the YOUTUBE_API_KEY variable, you must write youtube api key obtained at console.developers.google.com
4. You need to build a Docker container
	Example of the Docker command (run in the project folder): docker build -t channel_parser:1.0.0 .
5. We start the created container with the parameter --env-file where it specifies the path to the file with environment variables and the -p parameter with the ip address and port
	An example of a container launch command: docker container run -d --env-file ./env.list -p 0.0.0.0:5000:5000 channel_parser:1.0.0
6. Send the request from the channel id to the ip address using the curl post method
	Example of a query command: curl -X POST -d "id=UCapxMWLnwKCFpEofs5433aA" 127.0.0.1:5000/


RUS:
Channel_parser это микросервис который сохраняет информацию о всех видео найденных наyoutube канале. Channel_parser принимает id yotube канала, обрабатывает запрос, записывает информацию о найденых видео в базу данных и передает задачу на скачивание микросервису Downloader если он доступен. Микросервисная архитектура позволяет запускать одновременно несколько Channel_parser для увеличения количества одновременно обрабатываемых запросов.

1. В сети должна быть база данных в которую будут производиться записи о найденых видео. Параметры подключения нужно записать в переменную среды DB_CONNECT_STR которая находится в файле env.list. Формат переменной: {DB_name}://{user}:{password}@{IP}:{Port}
	Пример запуска Postgresql в Docker (не рекомендуется запускать базу данных в докере): run --name postgresql -p 0.0.0.0:5432:5432 -e POSTGRES_PASSWORD=1 -d postgres
2. Для передачи найденых видео на скачивание в сети должен быть RabbitMQ. IP адресс RabbitMQ нужно записать в переменную среды PIKA_CONNECT_IP. Название очереди в переменную PIKA_QUEUE_NAME. Переменные находятся в файле env.list
	Пример запуска RabbitMQ в Docker: docker run -d --hostname my-rabbit --name rabbit -p 0.0.0.0:15672:15672 -p 0.0.0.0:5672:5672 rabbitmq:3-management
3. В файл env.list в переменную YOUTUBE_API_KEY нужно записать youtube api key полученый на сайте console.developers.google.com
4. Нужно построить Docker контейнер
	Пример команды Docker(запускаем в папке с проэктом): docker build -t channel_parser:1.0.0 . 
5. Запускаем созданый контейнер с параметром --env-file где указывает путь к файлу с переменными среды и параметром -p с указанием ip адресса и порта
	Пример команды запуска контейнера: docker container run -d --env-file ./env.list -p 0.0.0.0:5000:5000 channel_parser:1.0.0
6. Отправляем запрос с id канала на ip адрес методом curl post
	Пример команды запроса: curl -X POST -d "id=UCapxMWLnwKCFpEofs5433aA" 127.0.0.1:5000/