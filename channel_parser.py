#!/usr/bin/env python3
import os
import pika
import json
import logger
import urllib.request
from db import Db
from datetime import datetime
from flask import Flask, request


app = Flask(__name__)
db = Db()
api_key = os.environ['YOUTUBE_API_KEY'] # The key is taken from google developer
base_search_url = 'https://www.googleapis.com/youtube/v3/search?' # link to search video via Api

def link_generator(channel_id, next_page_token): # Function generates URL
    app.logger.info('>>> link generator')
    url = base_search_url + \
        'key={}&channelId={}&part=snippet,id&order=date&maxResults=20'.format(
            api_key, channel_id)

    if next_page_token is not None: # If there is a next page, create url
        url += f'&pageToken={next_page_token}'
    app.logger.info(f'> url:  {url}')
    return url


def youtube_request(url): # Get the request from YouTube
    app.logger.info('>>> youtube request')
    request = urllib.request.urlopen(url)
    return request.read()


def json_parser(inp): # Function searches for video in json five from youtube_request
    app.logger.info('>>> json parser')
    resp = json.loads(inp.decode('utf-8'))

    videos = []
    for i in resp['items']:
        if i['id']['kind'] == "youtube#video":
            app.logger.info(
                f"> Video found: {i['id']['videoId'], i['snippet']['title']}")
            videos.append([i['snippet']['channelId'], i['snippet']['channelTitle'], i['id']['videoId'],
                           i['snippet']['title'], i['snippet']['publishedAt'], i['snippet']['description']])

    if 'nextPageToken' in resp: # Search next page
        next_page_token = resp['nextPageToken']
    else:
        next_page_token = None

    app.logger.info(f'> Next page token {next_page_token}')
    return (videos, next_page_token)


def add_to_db(videos): # The function adds found videos to the database
    app.logger.info(f'>> add to db: {len(videos)}')
    videos_bd_id = []
    for video in videos:
        videos_bd_id.append(db.add_new_video(video[0], video[1], video[2],
                         video[3], video[4], video[5]))
    return videos_bd_id

def create_rmq_task(videos_bd_id): # The function creates tasks for downloader
    app.logger.info(f'>>> create rmq task: for {len(videos_bd_id)} videos')

    try:
        connection = pika.BlockingConnection(pika.ConnectionParameters(host = os.environ['PIKA_CONNECT_IP'], port = '5672'))
        channel = connection.channel()
        channel.queue_declare(queue=os.environ['PIKA_QUEUE_NAME'])
    except:
        app.logger.warning('Rabbitmq not activated')
        return

    for video in videos_bd_id:
        channel.basic_publish(exchange='', routing_key=os.environ['PIKA_QUEUE_NAME'], body = str(video))
        app.logger.info(f'> {video} was sented to queue')

    connection.close()

    app.logger.info(f"{len(videos_bd_id)} links  sent to the queue")
    return